package loa;

import ucb.gui.Pad;

import java.awt.Graphics2D;
import java.awt.Image;
import javax.imageio.ImageIO;

import java.io.InputStream;
import java.io.IOException;

import static loa.Board.*;

/** A widget that displays a Pinball playfield.
 *  @author Eva Zhang
 */
class GameDisplay extends Pad {

    /** Preferred dimensions of the playing surface. */
    private static final int BOARD_LEN = 644, SQUARE_LEN = 80;

    /** Displayed dimensions of a piece image. */
    private static final int PIECE_LEN = 70, ADJUST = 5;


    /** A graphical representation of GAME. */
    public GameDisplay(Game game) {
        _game = game;
        _board = game.getBoard();
        _start = new int[]{0, 0};
        setPreferredSize(BOARD_LEN, BOARD_LEN);
    }

    /** Returns starting point. */
    int[] getStart() {
        return _start;
    }

    /** Sets starting point at (COL, ROW). */
    void setStart(int col, int row) {
        _start[0] = col;
        _start[1] = row;
    }

    /** Return an Image read from the resource named NAME. */
    private Image getImage(String name) {
        InputStream in =
                getClass().getResourceAsStream("/loa/resources/" + name);
        try {
            return ImageIO.read(in);
        } catch (IOException excp) {
            return null;
        }
    }

    /** Returns an Image of piece with NAME. */
    private Image getPieceImage(String name) {
        return getImage(name + ".png");
    }

    /** Returns background of the game. */
    private Image getBackgroundImage() {
        return getImage("background.jpg");
    }

    /** Returns if clicked point (X, Y) is on current side. */
    boolean onPiece(int x, int y) {
        int col = (int) ((x - ADJUST) / SQUARE_LEN) + 1;
        int row = M - (int) ((y - ADJUST) / SQUARE_LEN);
        if (_board.get(col, row) == _board.turn()) {
            setStart(col, row);
            return true;
        }
        return false;
    }

    /** Returns a move at (X, Y) if eligible. */
    Move getMove(int x, int y) {
        int col = (int) ((x - ADJUST) / SQUARE_LEN) + 1;
        int row = M - (int) ((y - ADJUST) / SQUARE_LEN);
        return Move.create(_start[0], _start[1], col, row, _board);
    }

    /** Draws piece with NAME at COL, ROW on G. */
    private void paintPiece(Graphics2D g, String name, int col, int row) {
        if (name.equals("black") || name.equals("white")) {
            g.drawImage(getPieceImage(name), (col - 1) * SQUARE_LEN + ADJUST,
                    (M - row) * SQUARE_LEN + ADJUST,
                    PIECE_LEN, PIECE_LEN, null);
        }
    }

    /** Draw card back at X, Y on G. */
    private void paintBackground(Graphics2D g) {
        g.drawImage(getBackgroundImage(), 0, 0, BOARD_LEN, BOARD_LEN,
                    null);
    }

    /** Draw BOARD on G. */
    private void paintBoard(Graphics2D g) {
        for (int col = M; col > 0; col--) {
            for (int row = M; row > 0; row--) {
                Piece piece = _board.get(col, row);
                paintPiece(g, piece.fullName(), col, row);
            }
        }
    }

    @Override
    public synchronized void paintComponent(Graphics2D g) {
        paintBackground(g);
        paintBoard(g);
    }

    /** Game I am displaying. */
    private final Game _game;

    /** Board for the game. */
    private final Board _board;

    /** Starting column and row index. */
    private int[] _start = {0, 0};
}
