package loa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Formatter;
import java.util.HashSet;
import java.util.NoSuchElementException;

import java.util.regex.Pattern;

import static loa.Piece.*;
import static loa.Direction.*;

/** Represents the state of a game of Lines of Action.
 *  @author Eva Zhang
 */
class Board implements Iterable<Move> {

    /** Size of a board. */
    static final int M = 8;

    /** Pattern describing a valid square designator (cr). */
    static final Pattern ROW_COL = Pattern.compile("^[a-h][1-8]$");

    /** Starting index for columns and rows. */
    static final int INIT_INDEX = 1;
    /** A Board whose initial contents are taken from INITIALCONTENTS
     *  and in which the player playing TURN is to move. The resulting
     *  Board has
     *        get(col, row) == INITIALCONTENTS[row-1][col-1]
     *  Assumes that PLAYER is not null and INITIALCONTENTS is MxM.
     *
     *  CAUTION: The natural written notation for arrays initializers puts
     *  the BOTTOM row of INITIALCONTENTS at the top.
     */
    Board(Piece[][] initialContents, Piece turn) {
        initialize(initialContents, turn);
    }

    /** A new board in the standard initial position. */
    Board() {
        clear();
    }

    /** A Board whose initial contents and state are copied from
     *  BOARD. */
    Board(Board board) {
        copyFrom(board);
    }

    /** Set my state to CONTENTS with SIDE to move. */
    void initialize(Piece[][] contents, Piece side) {
        _moves.clear();
        for (int r = 1; r <= M; r += 1) {
            for (int c = 1; c <= M; c += 1) {
                set(c, r, contents[r - 1][c - 1]);
            }
        }
        _turn = side;
    }

    /** Set me to the initial configuration. */
    void clear() {
        initialize(INITIAL_PIECES, BP);
    }

    /** Set my state to a copy of BOARD. */
    void copyFrom(Board board) {
        if (board == this) {
            return;
        }
        initialize(board._curPieces, board._turn);
        _moves.addAll(board._moves);
    }

    /** Return the contents of column C, row R, where 1 <= C,R <= 8,
     *  where column 1 corresponds to column 'a' in the standard
     *  notation. */
    Piece get(int c, int r) {
        return _curPieces[r - 1][c - 1];
    }

    /** Return the contents of the square SQ.  SQ must be the
     *  standard printed designation of a square (having the form cr,
     *  where c is a letter from a-h and r is a digit from 1-8). */
    Piece get(String sq) {
        return get(col(sq), row(sq));
    }

    /** Returns the latest move. */
    Move getMove() {
        return _moves.get(movesMade() - 1);
    }

    /** Return the column number (a value in the range 1-8) for SQ.
     *  SQ is as for {@link get(String)}. */
    static int col(String sq) {
        if (!ROW_COL.matcher(sq).matches()) {
            throw new IllegalArgumentException("bad square designator");
        }
        return sq.charAt(0) - 'a' + 1;
    }

    /** Return the row number (a value in the range 1-8) for SQ.
     *  SQ is as for {@link get(String)}. */
    static int row(String sq) {
        if (!ROW_COL.matcher(sq).matches()) {
            throw new IllegalArgumentException("bad square designator");
        }
        return sq.charAt(1) - '0';
    }

    /** Set the square at column C, row R to V, and make NEXT the next side
     *  to move, if it is not null. */
    void set(int c, int r, Piece v, Piece next) {
        _curPieces[r - 1][c - 1] = v;
        if (next != null) {
            _turn = next;
        }
    }

    /** Set the square at column C, row R to V. */
    void set(int c, int r, Piece v) {
        set(c, r, v, null);
    }

    /** Assuming isLegal(MOVE), make MOVE. */
    void makeMove(Move move) {
        boolean ifLegal = isLegal(move);
        assert ifLegal;
        if (!ifLegal) {
            throw new IllegalArgumentException("illegal move");
        }
        _moves.add(move);
        Piece replaced = move.replacedPiece();
        int c0 = move.getCol0(), c1 = move.getCol1();
        int r0 = move.getRow0(), r1 = move.getRow1();
        if (replaced != EMP) {
            set(c1, r1, EMP);
        }
        set(c1, r1, move.movedPiece());
        set(c0, r0, EMP);
        _turn = _turn.opposite();
    }

    /** Retract (unmake) one move, returning to the state immediately before
     *  that move.  Requires that movesMade () > 0. */
    void retract() {
        assert movesMade() > 0;
        Move move = _moves.remove(_moves.size() - 1);
        Piece replaced = move.replacedPiece();
        int c0 = move.getCol0(), c1 = move.getCol1();
        int r0 = move.getRow0(), r1 = move.getRow1();
        Piece movedPiece = move.movedPiece();
        set(c1, r1, replaced);
        set(c0, r0, movedPiece);
        _turn = _turn.opposite();
    }

    /** Return the Piece representing who is next to move. */
    Piece turn() {
        return _turn;
    }

    /** Return true iff MOVE is legal for the player currently on move. */
    boolean isLegal(Move move) {
        if (move == null
                || _curPieces[move.getRow0() - 1][move.getCol0() - 1]
                        != _turn) {
            return false;
        }
        int dif;
        if (move.getCol0() == move.getCol1()) {
            dif = move.getRow0() - move.getRow1();
        } else {
            dif = move.getCol0() - move.getCol1();
        }
        if (pieceCountAlong(move) != ((dif > 0) ? dif : -dif)
                || blocked(move)) {
            return false;
        }
        return true;
    }

    /** Return a sequence of all legal moves from this position. */
    Iterator<Move> legalMoves() {
        return new MoveIterator();
    }

    @Override
    public Iterator<Move> iterator() {
        return legalMoves();
    }

    /** Return true if there is at least one legal move for the player
     *  on move. */
    public boolean isLegalMove() {
        return iterator().hasNext();
    }

    /** Return true iff either player has all his pieces contiguous. */
    boolean gameOver() {
        return piecesContiguous(BP) || piecesContiguous(WP);
    }

    /** Return true iff SIDE's pieces are contiguous. */
    boolean piecesContiguous(Piece side) {
        HashSet<Integer> visited = new HashSet<Integer>();
        int[] info = startLocal(side);
        return  countContiguousPieces(side, visited, info[0],
                info[1]) == info[2];
    }

    /** Returns a location containing SIDE and number of SIDE. */
    int[] startLocal(Piece side) {
        int startC = 0;
        int startR = 0;
        int count = 0;
        for (int row = 1; row <= M; row++) {
            for (int col = 1; col <= M; col++) {
                if (_curPieces[row - 1][col - 1] == side) {
                    if (startC == 0) {
                        startC = col;
                        startR = row;
                    }
                    count++;
                }
            }
        }
        return new int[]{startC, startR, count};
    }

    /** Returns a location containing SIDE excluding VISITED. Checks the board
     *  starting <OLDCOL, OLDROW>. */
    int[] startLocal(HashSet<Integer> visited, Piece side, int oldCol,
            int oldRow) {
        for (int row = oldRow; row <= M; row++) {
            for (int col = oldCol; col <= M; col++) {
                if (_curPieces[row - 1][col - 1] == side
                        && !visited.contains(10 * col + row)) {
                    return new int[]{col, row};
                }
            }
        }
        return null;
    }

    /** Return the maximum number of connected pieces for SIDE. */
    int maxConnected(Piece side) {
        HashSet<Integer> visited = new HashSet<Integer>();
        int max = 0;
        int[] startCR = new int[]{INIT_INDEX, INIT_INDEX};
        while (visited.size() < M * M) {
            startCR = startLocal(visited, side, startCR[0], startCR[1]);
            if (startCR == null) {
                break;
            }
            int newCount = countContiguousPieces(side, visited,
                    startCR[0], startCR[1]);
            if (max < newCount) {
                max = newCount;
            }
        }
        return max;
    }

    /** Returns the total number contiguous pieces for SIDE excluding VISITED.
     *  Start counting from (STARTC, STARTR). */
    int countContiguousPieces(Piece side, HashSet<Integer> visited,
            int startC, int startR) {
        int count = 1;
        Direction[] dirList = {N, S, W, E, NW, NE, SW, SE};
        visited.add(10 * startC + startR);
        for (Direction d: dirList) {
            int row = startR + d.dr;
            int col = startC + d.dc;
            if (row >= 1 && row <= M
                    && col >= 1 && col <= M
                    && _curPieces[row - 1][col - 1] == side) {
                int newPiece = 10 * col + row;
                if (!visited.contains(newPiece)) {
                    visited.add(10 * col + row);
                    count += countContiguousPieces(side, visited,
                            col, row);
                }
            }
        }
        return count;
    }

    /** Return the total number of moves that have been made (and not
     *  retracted).  Each valid call to makeMove with a normal move increases
     *  this number by 1. */
    int movesMade() {
        return _moves.size();
    }

    @Override
    public boolean equals(Object obj) {
        Board b = (Board) obj;
        return Arrays.deepEquals(_curPieces, b._curPieces)
                && _turn == b._turn;
    }

    @Override
    public int hashCode() {
        int result = 0;
        for (Piece[] pList: _curPieces) {
            int subResult = 0;
            for (Piece p: pList) {
                subResult <<= 1;
                subResult += (p.hashCode() % 5) / 2;
            }
            result <<= 1;
            result += subResult;
        }
        result <<= 2;
        result += (_turn.hashCode() % 5) / 2;
        return result;
    }

    @Override
    public String toString() {
        Formatter out = new Formatter();
        out.format("===%n");
        for (int r = M; r >= 1; r -= 1) {
            out.format("    ");
            for (int c = 1; c <= M; c += 1) {
                out.format("%s ", get(c, r).abbrev());
            }
            out.format("%n");
        }
        out.format("Next move: %s%n===", turn().fullName());
        return out.toString();
    }

    /** Returns the content of board. */
    public String display() {
        Formatter out = new Formatter();
        for (int r = M; r >= 1; r -= 1) {
            out.format("%d ", r);
            for (int c = 1; c <= M; c += 1) {
                out.format("%s ", get(c, r).abbrev());
            }
            out.format("%n");
        }
        out.format("  a b c d e f g h   ");
        out.format(turn().fullName());
        out.format(" to move");
        return out.toString();
    }

    /** Return the number of pieces in the line of action indicated by MOVE. */
    private int pieceCountAlong(Move move) {
        return pieceCountAlong(move.getCol0(), move.getRow0(),
                move.getDirection());
    }

    /** Return the number of pieces in the line of action in direction DIR and
     *  containing the square at column C and row R. */
    private int pieceCountAlong(int c, int r, Direction dir) {
        if (dir == NOWHERE) {
            return 0;
        }
        int count = 0;
        int dc = dir.dc;
        int dr = dir.dr;
        for (int row = r, col = c; row > 0 && col > 0 && row < 9 && col < 9;
                row += dr, col += dc) {
            if (_curPieces[row - 1][col - 1] != EMP) {
                count++;
            }
        }
        for (int row = r, col = c; row > 0 && col > 0 && row < 9 && col < 9;
                row -= dr, col -= dc) {
            if (_curPieces[row - 1][col - 1] != EMP) {
                count++;
            }
        }
        return count - 1;
    }

    /** Return true iff MOVE is blocked by an opposing piece or by a
     *  friendly piece on the target square. */
    private boolean blocked(Move move) {
        int col0 = move.getCol0();
        int col1 = move.getCol1();
        int row0 = move.getRow0();
        int row1 = move.getRow1();
        Piece p0 = _curPieces[row0 - 1][col0 - 1];
        Piece p1 = _curPieces[row1 - 1][col1 - 1];

        if (p0 == p1) {
            return true;
        }
        Direction dir = move.getDirection();
        int dr = dir.dr;
        int dc = dir.dc;
        Piece oppo = p0.opposite();
        for (row0 += dr, col0 += dc; row0 != row1 || col0 != col1;
                row0 += dr, col0 += dc) {
            if (_curPieces[row0 - 1][col0 - 1] == oppo) {
                return true;
            }
        }
        return false;
    }

    /** Return moves. */
    ArrayList<Move> getMoves() {
        return _moves;
    }

    /** The standard initial configuration for Lines of Action. */
    static final Piece[][] INITIAL_PIECES = {
            { EMP, BP,  BP,  BP,  BP,  BP,  BP,  EMP },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
            { EMP, BP,  BP,  BP,  BP,  BP,  BP,  EMP }
    };

    /** List of all unretracted moves on this board, in order. */
    private final ArrayList<Move> _moves = new ArrayList<>();
    /** Current side on move. */
    private Piece _turn;
    /** Current game configuration. */
    private Piece[][] _curPieces = new Piece[M][M];

    /** An iterator returning the legal moves from the current board. */
    private class MoveIterator implements Iterator<Move> {
        /** Current piece under consideration. */
        private int _c, _r;
        /** Next direction of current piece to return. */
        private Direction _dir;
        /** Next move. */
        private Move _move;

        /** A new move iterator for turn(). */
        MoveIterator() {
            _c = M; _r = M; _dir = NOWHERE;
            incr();
        }

        @Override
        public boolean hasNext() {
            return _move != null;
        }

        @Override
        public Move next() {
            if (_move == null) {
                throw new NoSuchElementException("no legal move");
            }

            Move move = _move;
            incr();
            return move;
        }

        @Override
        public void remove() {
        }

        /** Advance to the next legal move. */
        private void incr() {
            while (_turn != _curPieces[_r - 1][_c - 1] || _dir == null) {
                if (_c != 1) {
                    _c--;
                } else if (_r != 1) {
                    _r--;
                    _c = M;
                } else {
                    _move = null;
                    return;
                }
                _dir = NOWHERE;
            }
            while (true) {
                _dir = _dir.succ();
                while (_dir != null) {
                    int numMoves = pieceCountAlong(_c, _r, _dir);
                    _move = Move.create(_c, _r, _c + numMoves * _dir.dc,
                            _r + numMoves * _dir.dr, Board.this);
                    if (isLegal(_move)) {
                        return;
                    }
                    _dir = _dir.succ();
                }
                while (_turn != _curPieces[_r - 1][_c - 1] || _dir == null) {
                    if (_c != 1) {
                        _c--;
                    } else if (_r != 1) {
                        _r--;
                        _c = M;
                    } else {
                        _move = null;
                        return;
                    }
                    _dir = NOWHERE;
                }
            }
        }
    }
}
