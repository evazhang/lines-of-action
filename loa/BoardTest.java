package loa;
import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Test;

import static loa.Piece.*;
import static loa.Direction.*;
import static loa.Board.*;

/** Perform tests of the Board class
 * @author Eva Zhang. */
public class BoardTest {
    /** Tests initialize operation and constructor. */
    @Test
    public void testInitialize() {
        Board b = new Board(INITIAL_PIECES, BP);
        for (int row = 1; row <= M; row++) {
            for (int col = 1; col <= M; col++) {
                assertEquals(String.format("row {0}, col {1}", row, col),
                        b.get(col, row), INITIAL_PIECES[row - 1][col - 1]);
            }
        }
    }

    /** Tests set operation. */
    @Test
    public void testSet() {
        Board b = new Board(INITIAL_PIECES, BP);
        assertEquals(b.get(1, 8), EMP);
        b.set(1, 8, BP);
        assertEquals(b.get(1, 8), BP);
    }

    /** Tests isLegal, makeMove, and movesMade operations. */
    @Test
    public void testIsLegalMakeMoveMovesMade() {
        Board b = new Board(INITIAL_PIECES, BP);
        assertEquals(Move.create("a1-b2", b), null);

        Move m1 = Move.create("b1-d3", b);
        assertEquals(m1.getDirection(), NE);

        b.makeMove(m1);
        assertEquals(1, b.movesMade());

        assertEquals(b.isLegal(Move.create("a4-d1", b)), false);
        assertEquals(b.isLegal(Move.create("a4-c2", b)), true);
        assertEquals(b.isLegal(Move.create("a4-b3", b)), false);
        assertEquals(b.isLegal(Move.create("a5-c5", b)), true);
        assertEquals(b.isLegal(Move.create("a5-c6", b)), false);
        assertEquals(b.isLegal(Move.create("a5-c7", b)), true);
        assertEquals(b.isLegal(Move.create("a5-c8", b)), false);

        b.makeMove(Move.create("a5-c5", b));

        b.makeMove(Move.create("e1-e3", b));

        b.makeMove(Move.create("h5-f5", b));

        b.makeMove(Move.create("c8-a6", b));

        assertEquals(b.isLegal(Move.create("e3-g3", b)), false);

        b.makeMove(Move.create("h7-f7", b));
        assertEquals(6, b.movesMade());


        assertEquals(b.isLegal(Move.create("e3-g3", b)), false);
        assertEquals(b.isLegal(Move.create("e3-c3", b)), false);
        assertEquals(b.isLegal(Move.create("e3-a3", b)), true);
        assertEquals(b.isLegal(Move.create("e3-b3", b)), false);
        assertEquals(b.isLegal(Move.create("e3-c3", b)), false);
        assertEquals(b.isLegal(Move.create("e3-g5", b)), false);
        assertEquals(b.isLegal(Move.create("e3-h6", b)), true);

        Piece[][] testPieces = {
                { EMP, BP, BP, BP, BP, BP, BP, EMP },
                { WP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, WP, EMP, EMP, EMP, EMP, WP },
                { WP, EMP, EMP, EMP, EMP, WP, EMP, EMP },
                { EMP, EMP, WP, EMP, EMP, EMP, EMP, WP },
                { BP, BP, EMP, BP, EMP, EMP, EMP, WP },
                { WP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
                { BP, EMP, EMP, EMP, EMP, BP, BP, WP }
        };
        Board b2 = new Board(testPieces, BP);
        assertEquals(b2.isLegal(Move.create("a8-a3", b2)), false);
    }

    /** Tests piecesContiguous and maxConnected operations. */
    @Test
    public void testPiecesContiguousMaxConnected() {
        Piece[][] initialBoard = {
                { EMP, EMP, EMP, BP, EMP, EMP, EMP, EMP },
                { EMP, WP, WP, BP, EMP, EMP, EMP, EMP },
                { EMP, EMP, BP, BP,  WP, WP, EMP, WP },
                { EMP, WP, BP, WP, WP, EMP, EMP, EMP },
                { EMP, BP, WP, BP, BP, BP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP }
        };
        Piece[][] onePiece = {
                { EMP, BP, BP, BP, BP, BP, BP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, WP },
                { EMP, BP, BP, BP, BP, BP, BP, EMP }
        };
        Board b = new Board(initialBoard, BP);
        assertEquals(b.piecesContiguous(BP), true);
        assertEquals(b.piecesContiguous(WP), false);
        assertEquals(b.maxConnected(BP), 9);
        assertEquals(b.maxConnected(WP), 6);
        b.set(7, 3, WP);
        assertEquals(b.maxConnected(WP), 8);
        b.set(4, 2, WP);
        assertEquals(b.piecesContiguous(WP), true);
        assertEquals(b.piecesContiguous(BP), false);
        assertEquals(b.maxConnected(WP), 11);
        assertEquals(b.maxConnected(BP), 7);
        b.set(1, 8, WP);
        assertEquals(b.piecesContiguous(WP), false);
        assertEquals(b.maxConnected(WP), 11);
        assertEquals(b.maxConnected(BP), 7);
        b = new Board(onePiece, BP);
        assertEquals(b.piecesContiguous(WP), true);
        assertEquals(b.piecesContiguous(BP), false);
        assertEquals(b.maxConnected(WP), 1);
        assertEquals(b.maxConnected(BP), 6);
        b.set(8, 5, WP);
        assertEquals(b.piecesContiguous(WP), false);
        assertEquals(b.piecesContiguous(BP), false);
        assertEquals(b.maxConnected(WP), 1);
        assertEquals(b.maxConnected(BP), 6);
        b.set(8, 6, WP);
        assertEquals(b.piecesContiguous(WP), true);
        assertEquals(b.piecesContiguous(BP), false);
        assertEquals(b.maxConnected(WP), 3);
        assertEquals(b.maxConnected(BP), 6);
        b.set(5, 2, BP);
        assertEquals(b.piecesContiguous(WP), true);
        assertEquals(b.piecesContiguous(BP), false);
        assertEquals(b.maxConnected(WP), 3);
        assertEquals(b.maxConnected(BP), 7);
    }

    /** Tests MoveIterator operation. */
    @Test
    public void testMoveIterator() {
        Board b = new Board();
        b.clear();

        Iterator<Move> iter1 = b.legalMoves();
        while (iter1.hasNext()) {
            Move m = iter1.next();
            b.makeMove(m);
            b.retract();
        }
        b.makeMove(Move.create("b1-b3", b));
        Iterator<Move> iter2 = b.legalMoves();
        while (iter2.hasNext()) {
            Move m = iter2.next();
            b.makeMove(m);

            b.retract();
        }
    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(BoardTest.class));
    }
}
