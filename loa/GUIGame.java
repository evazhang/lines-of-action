package loa;

import static loa.Piece.BP;

import javax.swing.JOptionPane;

/** A type of game that gets input from the mouse, and reports
 *  game positions and reports errors on a GUI.
 *  @author Eva Zhang
 */
class GUIGame extends Game {

    /** A GUIPlayer that makes moves on GAME. */
    GUIGame() {
        super();
        setBoard(new Board());
        _board = this.getBoard();
    }

    @Override
    public void play() {
        _display = new GameGUI("Line of Legend", this);
        setPlaying(true);
        _display.paint();
    }

    /** Announce winner. */
    void announceWinner() {
        Piece lastTurn = _board.turn().opposite();
        String message;
        if (_board.piecesContiguous(lastTurn)) {
            message = (lastTurn == BP) ? "Black" : "White"
                    + " wins.";
        } else {
            message = (lastTurn == BP) ? "White" : "Black"
                    + " wins.";
        }
        JOptionPane.showMessageDialog(null, message);
    }
    /** Board. */
    private Board _board;

    /** Displays the playing surface. */
    private GameGUI _display;
}
