package loa;

import java.util.ArrayList;
import java.util.Iterator;

/** An automated Player.
 *  @author Eva Zhang*/
class MachinePlayer extends Player {

    /** Value for wining or losing the game. */
    static final int WON = Integer.MAX_VALUE, LOSE = -Integer.MAX_VALUE;
    /** Repeating Gap. */
    static final int REPEAT_GAP = 4;
    /** Depth for the game tree. */
    static final int DEPTH = 3;

    /** A MachinePlayer that plays the SIDE pieces in GAME. */
    MachinePlayer(Piece side, Game game) {
        super(side, game);
    }

    @Override
    Move makeMove() {
        Game game = getGame();
        Board board = getBoard();
        Piece side = side();

        Data moveData = findBestMove(side, board, DEPTH, WON);
        Move bestMove = moveData.getMove();
        bestMove = (bestMove == null) ? getRanMove(board, game) : bestMove;

        int numMoves = board.movesMade();
        ArrayList<Move> moves = board.getMoves();
        if (numMoves > REPEAT_GAP * REPEAT_GAP
                && bestMove == moves.get(numMoves - REPEAT_GAP)) {
            if (bestMove == moves.get(numMoves - REPEAT_GAP * 2)) {
                if (bestMove == moves.get(numMoves - REPEAT_GAP * 3)) {
                    bestMove = getRanMove(board, game);
                }
            }
        }
        getGame().ifDisplay(game.ifAutoPrint());
        System.out.println(board.turn().abbrev().toUpperCase()
                + "::" + bestMove);
        return bestMove;
    }

    /** Returns a random move from GAME and BOARD. */
    Move getRanMove(Board board, Game game) {
        Iterator<Move> iterRan = board.legalMoves();
        int count = game.randInt(10);
        while (count >= 0 && iterRan.hasNext()) {
            return iterRan.next();
        }
        return null;
    }

    /** Return data for a legal move for SIDE from BOARD that either has an
     *  estimated value >= CUTOFF or that has the best estimated value for
     *  player SIDE, Looking up to DEPTH moves ahead, stop when value is
     *  larger than CUTOFF. */
    Data findBestMove(Piece side, Board board, int depth, int cutoff) {
        Iterator<Move> iter = board.legalMoves();
        if (board.piecesContiguous(side.opposite())) {
            return new Data(null, LOSE);
        } else if (board.piecesContiguous(side)) {
            return new Data(null, WON);
        } else if (depth == 0) {
            return guessBestMove(iter, side, board, cutoff);
        }
        Data bestDataSoFar = new Data(null, LOSE);
        while (iter.hasNext()) {
            Move move = iter.next();
            board.makeMove(move);
            int bsfVal = bestDataSoFar.getValue();
            Data response = findBestMove(side.opposite(), board,
                    depth - 1, -bsfVal);
            int reVal = response.getValue();
            board.retract();
            if (-reVal > bsfVal) {
                bestDataSoFar = new Data(move, -reVal);
                if (-reVal >= cutoff) {
                    break;
                }
            }
        }
        return bestDataSoFar;
    }

    /** Returns data for a legal move for SIDE from ITER and BOARD. Stop when
     *  value is larger than CUTOFF. */
    Data guessBestMove(Iterator<Move> iter, Piece side, Board board,
            int cutoff) {
        Data bestDataSoFar = new Data(null, LOSE);
        while (iter.hasNext()) {
            Data moveData = getData(iter, board);
            int moveVal = moveData.getValue();
            if (moveVal > bestDataSoFar.getValue()) {
                bestDataSoFar = moveData;
                if (moveVal >= cutoff) {
                    break;
                }
            }
        }
        return bestDataSoFar;
    }

    /** Returns value of BOARD. The higher, the better from current turn. */
    int evaluate(Board board) {
        Piece enemy = board.turn();
        Piece me = enemy.opposite();
        if (board.piecesContiguous(enemy)) {
            return LOSE;
        }
        if (board.piecesContiguous(me)) {
            return WON;
        }
        return board.maxConnected(me) - board.maxConnected(enemy);
    }

    /** Returns value of a move from ITER and BOARD. */
    Data getData(Iterator<Move> iter, Board board) {
        Move move = iter.next();
        board.makeMove(move);
        int val = evaluate(board);
        board.retract();
        return new Data(move, val);
    }
}
