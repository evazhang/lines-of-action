package loa;
import static org.junit.Assert.*;

import org.junit.Test;

import static loa.Piece.*;

/** Perform tests of the MachinePlayer class
 *  @author Eva Zhang. */
public class MachinePlayerTest {

    /** Tests AI. */
    @Test
    public void testAI() {
        Piece[][] testPieces = {
                { EMP, BP,  BP,  BP,  EMP, EMP, EMP,  EMP },
                { EMP, EMP, EMP, EMP, EMP, EMP, EMP, EMP },
                { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
                { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
                { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
                { WP,  EMP, EMP, EMP, EMP, EMP, EMP, WP  },
                { WP,  BP,  BP,  BP,  BP,  BP,  BP,  WP  },
                { WP,  EMP, EMP, EMP, BP,  BP,  BP, WP  }
        };

        Board board = new Board(testPieces, BP);
        Game game = new Game(board);
        MachinePlayer player = new MachinePlayer(BP, game);
        Move move = player.makeMove();
        assertEquals(move.toString(), "e8-e6");
        board.makeMove(move);
        move = player.makeMove();
        assertEquals(move.toString(), "h3-e6");
        board.makeMove(move);
        move = player.makeMove();
        assertEquals(move.toString(), "g8-g6");
    }

    public static void main(String[] args) {
        System.exit(ucb.junit.textui.runClasses(MachinePlayerTest.class));
    }
}
