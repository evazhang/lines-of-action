package loa;

/** Represents the data for each move in game tree.
 *  @author Eva Zhang
 */
public class Data {

    /** A new data object with MOVE and its VALUE. */
    public Data(Move move, int value) {
        _move = move;
        _value = value;
    }

    /** Return move. */
    Move getMove() {
        return _move;
    }

    /** Return value for the move. */
    int getValue() {
        return _value;
    }

    /** Current move. */
    private Move _move;
    /** Value of current move branch. */
    private int _value;
}
