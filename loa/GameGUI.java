package loa;

import ucb.gui.TopLevel;
import ucb.gui.LayoutSpec;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/** A top-level GUI for Lines of Legend.
 *  @author Eva Zhang
 */
class GameGUI extends TopLevel {

    /** A new window with given TITLE and displaying GAME. */
    GameGUI(String title, GUIGame game) {
        super(title, true);
        _game = game;
        _display = new GameDisplay(game);
        _board = _game.getBoard();

        add(_display, new LayoutSpec("y", 2, "width", 2));
        addLabel("You are black. Click the piece you want to move first, and"
                + " then click its destination. ", "mes",
                new LayoutSpec("y", 3, "x", 0));

        makeMenuBar();

        _display.setMouseHandler("click", this, "mouseClicked");

        display(true);
    }

    /** Set up menu bar and all menu items included. */
    public void makeMenuBar() {
        JMenuBar menuBar = new JMenuBar();
        this.frame.setJMenuBar(menuBar);

        JMenu menu = new JMenu("Game");
        menuBar.add(menu);

        JMenuItem newGameItem = new JMenuItem("New Game");
        newGameItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
                ActionEvent.SHIFT_MASK
                + ActionEvent.CTRL_MASK));
        newGameItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLabel("mes", "started a new game");
                newGame("");
            }
        });
        menu.add(newGameItem);

        JMenuItem undoItem = new JMenuItem("Undo");
        undoItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,
                ActionEvent.SHIFT_MASK
                + ActionEvent.CTRL_MASK));
        undoItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLabel("mes", "undid the previous step");
                undo("");
            }
        });
        menu.add(undoItem);

        JMenuItem quitItem = new JMenuItem("Quit");
        quitItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,
                ActionEvent.SHIFT_MASK
                + ActionEvent.CTRL_MASK));
        quitItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                quit("");
            }
        });
        menu.add(quitItem);
    }

    /** Response to "New Game" menu item. */
    public void newGame(String dummy) {
        _board.clear();
        _display.repaint();
        _game.play();
    }

    /** Response to "Undo" menu item. */
    public void undo(String dummy) {
        _board.retract();
        _board.retract();
        _game.setPlaying(true);
        _display.repaint();
    }

    /** Respond to "Quit" menu item. */
    public void quit(String dummy) {
        if (showOptions("Really quit?", "Quit?", "question",
                "Yes", "Yes", "No") == 0) {
            System.exit(0);
        }
    }

    /** Action in response to mouse-clicking event EVENT. */
    public synchronized void mouseClicked(MouseEvent event) {
        int x = event.getX(), y = event.getY();
        int[] start = _display.getStart();
        if (start[0] == 0 && start[1] == 0) {
            if (_display.onPiece(x, y)) {
                setLabel("mes", "set starting point");
            } else {
                setLabel("mes", "wrong starting point");
            }
        } else {
            Move move = _display.getMove(x, y);
            if (!_board.isLegal(move)) {
                move = null;
                setLabel("mes", "illegal move");
            } else {
                assert _board.isLegal(move);
                _board.makeMove(move);
                _display.repaint();
                move = null;
                if (_game.isPlaying()) {
                    if (_board.gameOver()) {
                  
                        _game.announceWinner();
                        _game.setPlaying(false);
                        setLabel("mes", "Game over. Shift + Control + N "
                                + "to restart");
                    }
                    move = _game.getPlayers()[1].makeMove();
                } else {
                    System.out.println("Not playing...");
                }
                if (move != null) {
                    assert _board.isLegal(move);
                    _board.makeMove(move);
                    _display.repaint();
                    setLabel("mes", "opponent made a move");
                    if (_board.gameOver()) {
                        _game.announceWinner();
                        _game.setPlaying(false);
                        setLabel("mes", "Game over. Shift + Control + N "
                                + "to restart");
                    }
                }
            }
            _display.setStart(0, 0);
        }
    }

    /** Repaints the game. */
    public void paint() {
        _display.repaint();
    }

    /** The board widget. */
    private final GameDisplay _display;

    /** The board. */
    private Board _board;

    /** The game I am consulting. */
    private final GUIGame _game;
}
